import React from 'react';
import PropTypes from 'prop-types';
import { Line } from 'react-chartjs-2';

import '../../../public/stylesheets/app.css';

const style = getComputedStyle(document.body);
const firstColor = style.getPropertyValue('--first-color');
const secondColor = style.getPropertyValue('--second-color');
const chartHeight = Number(style.getPropertyValue('--chart-height'));

const lineData = {
  labels: [],
  datasets: [
    {
      label: 'Nota media para los filtros seleccionados',
      fill: false,
      lineTension: 0.5,
      backgroundColor: firstColor,
      borderColor: firstColor,
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: secondColor,
      pointBackgroundColor: secondColor,
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: firstColor,
      pointHoverBorderColor: firstColor,
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      data: []
    }
  ]
};

class Chart extends React.Component {
  prepareRawData() {
    const { loading, data } = this.props;
    if (loading) {
      lineData.datasets[0].data = lineData.datasets[0].data.map(obj => 0);
    } else {
      const red = data.reduceRight((agg, obj) => {
        if (!agg.has(obj.date)) {
          agg.set(obj.date, {
            mark: Number(obj.mark),
            count: 1
          });
        } else {
          const curr = agg.get(obj.date);
          curr.count += 1;
          curr.mark += Number(obj.mark);
        }
        return agg;
      }, new Map());

      const chartLabels = [];
      const chartData = [];
      red.forEach((value, key) => {
        chartLabels.push(key);
        chartData.push(value.mark / value.count);
      });

      lineData.labels = chartLabels;
      lineData.datasets[0].data = chartData;
    }
  }

  render() {
    this.prepareRawData();
    return (
      <div>
        <Line data={lineData} height={chartHeight} options={{ maintainAspectRatio: false }} />
      </div>
    );
  }
}

Chart.propTypes = {
  data: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired
};

export default Chart;
