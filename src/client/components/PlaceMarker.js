import React, { Component } from 'react';
import PropTypes from 'prop-types';

import '../../../public/stylesheets/app.css';

const style = getComputedStyle(document.body);
const firstColor = style.getPropertyValue('--first-color');
const secondColor = style.getPropertyValue('--second-color');

const K_WIDTH = 40;
const K_HEIGHT = 40;

const markerStyle = {
  position: 'absolute',
  width: K_WIDTH,
  height: K_HEIGHT,
  left: -K_WIDTH / 2,
  top: -K_HEIGHT / 2,

  border: `3px solid ${firstColor}`,
  borderRadius: K_HEIGHT,
  backgroundColor: 'white',
  textAlign: 'center',
  color: secondColor,
  fontSize: 11,
  fontWeight: 'bold',
  padding: 8,
  paddingLeft: '1px'
};

const markerStyleHover = {
  ...markerStyle,
  border: '3px solid #f44336',
  zIndex: 1000
};

const hintStyle = {
  position: 'absolute',
  left: '105%',
  top: '-50%',
  width: '75px',
  display: 'inline-block',
  border: `1px solid ${firstColor}`,
  backgroundColor: 'rgba(235, 245, 255, 0.85)',
  borderRadius: 4,
  color: secondColor,
  textAlign: 'center'
};

class PlaceMarker extends Component {
  render() {
    const styleToApply = this.props.$hover ? markerStyleHover : markerStyle;
    const hint = this.props.$hover ? <div style={hintStyle}>{this.props.textHint}</div> : '';

    return (
      <div style={styleToApply}>
        <div>{this.props.text}</div>
        {hint}
      </div>
    );
  }
}

PlaceMarker.propTypes = {
  text: PropTypes.string.isRequired,
  textHint: PropTypes.string.isRequired,
  $hover: PropTypes.bool
};

export default PlaceMarker;
