import React from 'react';
import PropTypes from 'prop-types';
import GoogleMapReact from 'google-map-react';

import PlaceMarker from './PlaceMarker';

import '../../../public/stylesheets/app.css';

const style = getComputedStyle(document.body);
const mapHeight = Number(style.getPropertyValue('--map-height'));

class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultZoom: 7,
      defaultCenter: { lat: 37.35, lng: -4.3329005 }
    };
  }

  static mapData(data) {
    return data.reduce((agg, curr) => {
      const existent = agg.find(element => element.id == curr['unit.id']);
      if (existent === undefined) {
        agg.push({
          id: curr['unit.id'],
          name: curr['unit.name'],
          lat: curr['unit.lat'],
          lng: curr['unit.lon'],
          contracts: [
            {
              date: curr.date,
              mark: curr.mark,
              role: curr['role.name'],
              system: ['system.name'],
              type_of_personal: curr['type_of_personal.name'],
              duration: curr['duration.name']
            }
          ]
        });
      } else {
        existent.contracts.push({
          date: curr.date,
          mark: curr.mark,
          role: curr['role.name'],
          system: ['system.name'],
          type_of_personal: curr['type_of_personal.name'],
          duration: curr['duration.name']
        });
      }

      return agg;
    }, []);
  }

  render() {
    const { inputData } = this.props;
    const { defaultCenter, defaultZoom } = this.state;
    const data = Map.mapData(inputData);
    const rend = data.map(curr => (
      <PlaceMarker
        lat={curr.lat}
        lng={curr.lng}
        text={curr.contracts[0].mark.toString()}
        textHint={curr.name}
      />
    ));
    return (
      <div style={{ height: mapHeight, width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyDsUVRUdSlk1d2Bwg5KwCjOiQ2LOOUYx3Q' }}
          defaultCenter={defaultCenter}
          defaultZoom={defaultZoom}
        >
          {rend}
        </GoogleMapReact>
        <p id="hint-map">El mapa muestra el último contrato dado para los filtros seleccionados</p>
      </div>
    );
  }
}

Map.propTypes = {
  inputData: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired
};

export default Map;
