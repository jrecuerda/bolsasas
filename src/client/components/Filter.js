import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

import '../../../public/stylesheets/app.css';

const style = getComputedStyle(document.body);
const firstColor = style.getPropertyValue('--first-color');
const secondColor = style.getPropertyValue('--second-color');

class Filter extends React.Component {
  static mapData(data) {
    return data.map(current => ({ value: current.id, label: current.name }));
  }

  static mapValue(value, data) {
    return value.map((valueSelected) => {
      const optionSelected = data.find(currentOption => currentOption.id === valueSelected);
      return { value: optionSelected.id, label: optionSelected.name };
    });
  }

  constructor(props) {
    super(props);
    const { data, value } = this.props;
    this.state = {
      options: Filter.mapData(data),
      value: Filter.mapValue(value, data)
    };
    this.handleOnChange = this.handleOnChange.bind(this);
  }

  handleOnChange(value) {
    const { onchange, id, name } = this.props;
    this.setState({ value });
    onchange(id, name, value.map(currValue => currValue.value));
  }

  render() {
    const { name } = this.props;
    const { value, options } = this.state;
    const placeholder = `${name}`;

    const styleFn = {
      control: (provided, { isFocused }) => ({
        ...provided,
        borderColor: isFocused ? secondColor : firstColor,
        boxShadow: isFocused ? secondColor : firstColor,
        borderWidth: isFocused ? '2px' : '1px',
        ':hover': { borderColor: secondColor }
      }),
      option: (provided, { isFocused }) => ({
        ...provided,
        backgroundColor: isFocused ? firstColor : 'white',
        borderColor: secondColor
      }),
      multiValue: provided => ({
        ...provided,
        backgroundColor: firstColor,
        borderColor: secondColor
      }),
      multiValueLabel: provided => ({ ...provided, color: secondColor }),
      multiValueRemove: provided => ({ ...provided, color: secondColor }),
      clearIndicator: provided => ({ ...provided, color: secondColor }),
      dropdownIndicator: provided => ({ ...provided, color: secondColor })
    };
    const noOption = () => 'No hay coincidencias';

    return (
      <Select
        isMulti
        // joinvaValues
        value={value}
        selectedValue={value}
        placeholder={placeholder}
        options={options}
        onChange={this.handleOnChange}
        onBlurResetsInput={false}
        onCloseResetsInput={false}
        styles={styleFn}
        clearAllText="Eliminar todos"
        clearText="Eliminar"
        noOptionsMessage={noOption}
        closeMenuOnSelect={false}
      />
    );
  }
}

Filter.propTypes = {
  data: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  value: PropTypes.array.isRequired,
  onchange: PropTypes.func.isRequired
};

export default Filter;
