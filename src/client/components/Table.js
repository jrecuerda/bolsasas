import React from 'react';
import PropTypes from 'prop-types';
import ReactTable from 'react-table';

import 'react-table/react-table.css';

const columns = [
  {
    Header: 'Tipo de Contrato',
    id: 'type',
    show: true,
    accessor: d => d['type_of_personal.name']
  },
  {
    Header: 'Sistema',
    id: 'system',
    show: true,
    accessor: d => d['system.name']
  },
  {
    Header: 'Duración',
    id: 'duration',
    show: true,
    accessor: d => d['duration.name']
  },
  {
    Header: 'Puesto',
    id: 'role',
    show: true,
    accessor: d => d['role.name']
  },
  {
    Header: 'Unidad',
    id: 'unit',
    show: true,
    accessor: d => d['unit.name']
  },
  {
    Header: 'Nota',
    id: 'mark',
    show: true,
    accessor: d => d.mark
  },
  {
    Header: 'Fecha',
    id: 'date',
    show: true,
    accessor: d => d.date
  }
];

class Table extends React.Component {
  static setShowColumnTo(name, value) {
    columns.find(curr => curr.id === name).show = value;
  }

  setColumnsToShow() {
    const {
      showDuration, showSystem, showType, showRole, showUnit
    } = this.props;
    Table.setShowColumnTo('duration', showDuration);
    Table.setShowColumnTo('system', showSystem);
    Table.setShowColumnTo('type', showType);
    Table.setShowColumnTo('role', showRole);
    Table.setShowColumnTo('unit', showUnit);
  }

  calcPageSize() {
    const { data } = this.props;
    if (data.lengh === 0) {
      return 10;
    }
    if (data.lengh < 100) {
      return data.lengh;
    }
    return 100;
  }

  render() {
    this.setColumnsToShow();
    const pageSize = this.calcPageSize();
    const { data, loading } = this.props;
    return (
      <div className="table-wrap">
        <ReactTable
          className="-striped -highlight"
          data={data}
          columns={columns}
          defaultPageSize={pageSize}
          showPageSizeOptions={false}
          loading={loading}
          style={{ height: '500px' }}
          noDataText="Ups! No hay contratos!"
          previousText="Anterior"
          nextText="Siguiente"
          pageText="Página"
          ofText="de"
          rowsText="filas"
          loadingText="Cargando..."
        />
      </div>
    );
  }
}

Table.propTypes = {
  data: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  showSystem: PropTypes.bool.isRequired,
  showType: PropTypes.bool.isRequired,
  showDuration: PropTypes.bool.isRequired,
  showRole: PropTypes.bool.isRequired,
  showUnit: PropTypes.bool.isRequired
};

export default Table;
