import FetchApi from './fetchApi';

// TODO: Review warning
export function startFetching(actionType) {
  return function (dispatch) {
    return dispatch({ type: actionType });
  };
}

function fetchDataSuccess(actionType, data) {
  return { type: actionType, data };
}

export function fetchData(path, actionType) {
  return function (dispatch) {
    return FetchApi.fetch(path).then((data) => {
      dispatch(fetchDataSuccess(actionType, data));
    });
  };
}
