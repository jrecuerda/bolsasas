export default function filterChanged(actionType, value) {
  return function (dispatch) {
    dispatch({ type: actionType, value });
  };
}
