// TODO: Review this class
export default class FetchApi {
  static fetch(path) {
    return fetch(path)
      .then(response => response.json())
      .then(data => data);
  }
}
