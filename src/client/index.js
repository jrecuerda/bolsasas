// import React from 'react';
// import ReactDOM from 'react-dom';
// import App from './App';

// ReactDOM.render(<App />, document.getElementById('root'));

import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

import App from './containers/App';
import reducer from './reducers';

const middleware = [thunk];
if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger());
}

const parsePath = (path, name) => {
  let posField = path.search(name);
  if (posField === -1) {
    return [];
  }
  let sub = path.substr(posField + name.length + 1);
  posField = sub.search('&');
  if (posField !== -1) {
    sub = sub.substr(0, posField);
  }
  return sub.split(';').map(value => Number(value));
};

const path = window.location.search;
const filterDuration = parsePath(path, 'duration');
const filterSystem = parsePath(path, 'system');
const filterType = parsePath(path, 'type');
const filterRole = parsePath(path, 'role');
const filterUnit = parsePath(path, 'unit');

const store = createStore(reducer, applyMiddleware(...middleware));

const state = store.getState();
state.filters.duration.value = filterDuration;
state.filters.system.value = filterSystem;
state.filters.typeOfContract.value = filterType;
state.filters.role.value = filterRole;
state.filters.unit.value = filterUnit;

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
