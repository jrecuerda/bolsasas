import { combineReducers } from 'redux';
import {
  FETCH_ROLE,
  RECEIVE_ROLE,
  FETCH_UNIT,
  RECEIVE_UNIT,
  FETCH_SYSTEM,
  RECEIVE_SYSTEM,
  FETCH_TYPEOFCONTRACT,
  RECEIVE_TYPEOFCONTRACT,
  FETCH_DURATION,
  RECEIVE_DURATION
} from '../actions/actionTypes';

const roleReducer = (
  state = {
    isFetching: false,
    data: []
  },
  action
) => {
  switch (action.type) {
    case FETCH_ROLE:
      return {
        ...state,
        isFetching: true
      };
    case RECEIVE_ROLE:
      return {
        ...state,
        isFetching: false,
        data: action.data
      };
    default:
      return state;
  }
};

const unitReducer = (
  state = {
    isFetching: false,
    data: []
  },
  action
) => {
  switch (action.type) {
    case FETCH_UNIT:
      return {
        ...state,
        isFetching: true
      };
    case RECEIVE_UNIT:
      return {
        ...state,
        isFetching: false,
        data: action.data
      };
    default:
      return state;
  }
};

const systemReducer = (
  state = {
    isFetching: false,
    data: []
  },
  action
) => {
  switch (action.type) {
    case FETCH_SYSTEM:
      return {
        ...state,
        isFetching: true
      };
    case RECEIVE_SYSTEM:
      return {
        ...state,
        isFetching: false,
        data: action.data
      };
    default:
      return state;
  }
};

const typeOfContractReducer = (
  state = {
    isFetching: false,
    data: []
  },
  action
) => {
  switch (action.type) {
    case FETCH_TYPEOFCONTRACT:
      return {
        ...state,
        isFetching: true
      };
    case RECEIVE_TYPEOFCONTRACT:
      return {
        ...state,
        isFetching: false,
        data: action.data
      };
    default:
      return state;
  }
};
const durationReducer = (
  state = {
    isFetching: false,
    data: []
  },
  action
) => {
  switch (action.type) {
    case FETCH_DURATION:
      return {
        ...state,
        isFetching: true
      };
    case RECEIVE_DURATION:
      return {
        ...state,
        isFetching: false,
        data: action.data
      };
    default:
      return state;
  }
};

export default combineReducers({
  role: roleReducer,
  unit: unitReducer,
  system: systemReducer,
  typeOfContract: typeOfContractReducer,
  duration: durationReducer
});
