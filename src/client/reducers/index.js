import { combineReducers } from 'redux';

import initialDataReducer from './staticData';
import filtersReducer from './filters';
import contractsReducer from './contracts';

const rootReducer = combineReducers({
  initialData: initialDataReducer,
  filters: filtersReducer,
  contracts: contractsReducer
});

export default rootReducer;
