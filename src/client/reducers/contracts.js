import { FETCH_CONTRACTS, RECEIVE_CONTRACTS } from '../actions/actionTypes';

const modifyTypesFromContracs = contracts => contracts.map(contract => ({
  ...contract,
  // date: new Date(contract.date)
  mark: Number(contract.mark)
}));

export default (
  state = {
    isFetching: false,
    data: []
  },
  action
) => {
  switch (action.type) {
    case FETCH_CONTRACTS:
      return {
        ...state,
        isFetching: true
      };
    case RECEIVE_CONTRACTS:
      return {
        ...state,
        isFetching: false,
        data: modifyTypesFromContracs(action.data)
      };
    default:
      return state;
  }
};
