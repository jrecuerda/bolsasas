import { combineReducers } from 'redux';
import {
  FILTER_ROLE,
  FILTER_UNIT,
  FILTER_SYSTEM,
  FILTER_TYPEOFCONTRACT,
  FILTER_DURATION,
  FILTER_DATE_START,
  FILTER_DATE_END
} from '../actions/actionTypes';

const filterRoleReducer = (
  state = {
    value: []
  },
  action
) => {
  switch (action.type) {
    case FILTER_ROLE:
      return {
        ...state,
        value: action.value
      };
    default:
      return state;
  }
};

const filterUnitReducer = (
  state = {
    value: []
  },
  action
) => {
  switch (action.type) {
    case FILTER_UNIT:
      return {
        ...state,
        value: action.value
      };
    default:
      return state;
  }
};

const filterSystemReducer = (
  state = {
    value: []
  },
  action
) => {
  switch (action.type) {
    case FILTER_SYSTEM:
      return {
        ...state,
        value: action.value
      };
    default:
      return state;
  }
};

const filterTypeReducer = (
  state = {
    value: []
  },
  action
) => {
  switch (action.type) {
    case FILTER_TYPEOFCONTRACT:
      return {
        ...state,
        value: action.value
      };
    default:
      return state;
  }
};

const filterDurationReducer = (
  state = {
    value: []
  },
  action
) => {
  switch (action.type) {
    case FILTER_DURATION:
      return {
        ...state,
        value: action.value
      };
    default:
      return state;
  }
};

const filterStartDateReducer = (
  state = {
    value: []
  },
  action
) => {
  switch (action.type) {
    case FILTER_DATE_START:
      return {
        ...state,
        value: action.value
      };
    default:
      return state;
  }
};

const filterEndDateReducer = (
  state = {
    value: []
  },
  action
) => {
  switch (action.type) {
    case FILTER_DATE_END:
      return {
        ...state,
        value: action.value
      };
    default:
      return state;
  }
};

export default combineReducers({
  role: filterRoleReducer,
  unit: filterUnitReducer,
  system: filterSystemReducer,
  typeOfContract: filterTypeReducer,
  duration: filterDurationReducer,
  startDate: filterStartDateReducer,
  endDate: filterEndDateReducer
});
