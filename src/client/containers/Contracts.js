import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as fetchDataActions from '../actions/fetchData';
import { FETCH_CONTRACTS, RECEIVE_CONTRACTS } from '../actions/actionTypes';
import Table from '../components/Table';
import Chart from '../components/Chart';
import Map from '../components/Map';

class Contracts extends React.Component {
  componentDidMount() {
    const { path } = this.props;
    this.fetchContracts(path);
  }

  componentWillUpdate(nextProps) {
    const { path } = this.props;
    const { path: nextPath } = nextProps;
    if (path !== nextPath) {
      this.fetchContracts(nextPath);
    }
  }

  fetchContracts(path) {
    const { actions } = this.props;
    actions.fetch.startFetching(FETCH_CONTRACTS);
    actions.fetch.fetchData(path, RECEIVE_CONTRACTS);
  }

  render() {
    const { contracts, filters } = this.props;

    const showSystem = filters.system.value.length !== 1;
    const showDuration = filters.duration.value.length !== 1;
    const showType = filters.typeOfContract.value.length !== 1;
    const showRole = filters.role.value.length !== 1;
    const showUnit = filters.unit.value.length !== 1;

    return (
      <div className="Grid">
        <div className="Row">
          <Table
            loading={contracts.isFetching}
            data={contracts.data}
            showSystem={showSystem}
            showType={showType}
            showDuration={showDuration}
            showRole={showRole}
            showUnit={showUnit}
          />
        </div>
        <div className="Row">
          <Chart loading={contracts.isFetching} data={contracts.data} />
        </div>
        <div className="Row">
          <Map loading={contracts.isFetching} inputData={contracts.data} />
        </div>
      </div>
    );
  }
}

Contracts.propTypes = {
  path: PropTypes.string.isRequired,
  contracts: PropTypes.shape({
    isFetching: PropTypes.bool.isRequired,
    data: PropTypes.array.isRequired
  }).isRequired
};

const addFilter = (filters, name, data) => {
  if (data.length > 0) {
    filters.push(`${name}=${data.join(';')}`);
  }
};

const addFilterDate = (filters, name, data) => {
  if (data.length > 0) {
    filters.push(`${name}=${data}`);
  }
};

const buildPathFromFilters = (filters) => {
  const filtersPath = [];
  addFilter(filtersPath, 'system', filters.system.value);
  addFilter(filtersPath, 'duration', filters.duration.value);
  addFilter(filtersPath, 'type', filters.typeOfContract.value);
  addFilter(filtersPath, 'role', filters.role.value);
  addFilter(filtersPath, 'unit', filters.unit.value);
  addFilterDate(filtersPath, 'startDate', filters.startDate.value);
  addFilterDate(filtersPath, 'endDate', filters.endDate.value);

  let filterPart = '';
  if (filtersPath.length > 0) {
    filterPart += `?${filtersPath.join('&')}`;
  }

  return filterPart;
};

const mapStateToProps = (state) => {
  const filterPath = buildPathFromFilters(state.filters);
  // Modify current path
  window.history.pushState(null, null, `/${filterPath}`);
  return {
    path: `/contracts${filterPath}`,
    contracts: state.contracts,
    filters: state.filters
  };
};

const mapDispatchToProps = dispatch => ({
  actions: {
    fetch: bindActionCreators(fetchDataActions, dispatch)
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Contracts);
