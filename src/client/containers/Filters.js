import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as fetchDataActions from '../actions/fetchData';
import filterChanged from '../actions/filters';
import * as types from '../actions/actionTypes';
import Filter from '../components/Filter';
// import DatesFilter from '../components/DatesFilter';

const FilterTypes = new Map();
FilterTypes.set('role', {
  id: 1,
  name: 'Puesto',
  path: '/roles',
  fetchAction: types.FETCH_ROLE,
  receiveAction: types.RECEIVE_ROLE,
  filterAction: types.FILTER_ROLE
});
FilterTypes.set('unit', {
  id: 2,
  name: 'Unidad',
  path: '/units',
  fetchAction: types.FETCH_UNIT,
  receiveAction: types.RECEIVE_UNIT,
  filterAction: types.FILTER_UNIT
});
FilterTypes.set('type', {
  id: 3,
  name: 'Tipo de contrato',
  path: '/typeofpersonal',
  fetchAction: types.FETCH_TYPEOFCONTRACT,
  receiveAction: types.RECEIVE_TYPEOFCONTRACT,
  filterAction: types.FILTER_TYPEOFCONTRACT
});
FilterTypes.set('system', {
  id: 4,
  name: 'Sistema',
  path: '/system',
  fetchAction: types.FETCH_SYSTEM,
  receiveAction: types.RECEIVE_SYSTEM,
  filterAction: types.FILTER_SYSTEM
});
FilterTypes.set('duration', {
  id: 5,
  name: 'Duración',
  path: '/duration',
  fetchAction: types.FETCH_DURATION,
  receiveAction: types.RECEIVE_DURATION,
  filterAction: types.FILTER_DURATION
});

class Filters extends React.Component {
  constructor(props) {
    super(props);
    this.onFilterChanged = this.onFilterChanged.bind(this);
    this.onStartDateChange = this.onStartDateChange.bind(this);
    this.onEndDateChange = this.onEndDateChange.bind(this);
  }

  componentDidMount() {
    const { initialData } = this.props;
    this.fetchData(
      initialData.role,
      FilterTypes.get('role').path,
      FilterTypes.get('role').fetchAction,
      FilterTypes.get('role').receiveAction
    );
    this.fetchData(
      initialData.unit,
      FilterTypes.get('unit').path,
      FilterTypes.get('unit').fetchAction,
      FilterTypes.get('unit').receiveAction
    );
    this.fetchData(
      initialData.typeOfContract,
      FilterTypes.get('type').path,
      FilterTypes.get('type').fetchAction,
      FilterTypes.get('type').receiveAction
    );
    this.fetchData(
      initialData.system,
      FilterTypes.get('system').path,
      FilterTypes.get('system').fetchAction,
      FilterTypes.get('system').receiveAction
    );
    this.fetchData(
      initialData.duration,
      FilterTypes.get('duration').path,
      FilterTypes.get('duration').fetchAction,
      FilterTypes.get('duration').receiveAction
    );
  }

  onFilterChanged(id, name, value) {
    const { actions } = this.props;
    FilterTypes.forEach((currFilter, key) => {
      if (currFilter.id === id) {
        actions.filter(currFilter.filterAction, value);
      }
    });
  }

  onStartDateChange(value) {
    const { actions } = this.props;
    actions.filter(types.FILTER_DATE_START, value);
  }

  onEndDateChange(value) {
    const { actions } = this.props;
    actions.filter(types.FILTER_DATE_END, value);
  }

  getFilter(item, value, id, name) {
    const loadingText = (
      <p>
        Cargando
        {name}
        ...
      </p>
    );
    if (item.data.length === 0) {
      return loadingText;
    }
    return (
      <Filter data={item.data} name={name} id={id} value={value} onchange={this.onFilterChanged} />
    );
  }

  fetchData(object, path, fetchAction, receiveAction) {
    const { actions } = this.props;
    if (object.data.length === 0 && !object.isFetching) {
      actions.fetch.startFetching(fetchAction);
      actions.fetch.fetchData(path, receiveAction);
    }
  }

  render() {
    const { initialData, filters } = this.props;
    const duration = this.getFilter(
      initialData.duration,
      filters.duration.value,
      FilterTypes.get('duration').id,
      FilterTypes.get('duration').name
    );
    const role = this.getFilter(
      initialData.role,
      filters.role.value,
      FilterTypes.get('role').id,
      FilterTypes.get('role').name
    );
    const unit = this.getFilter(
      initialData.unit,
      filters.unit.value,
      FilterTypes.get('unit').id,
      FilterTypes.get('unit').name
    );
    const typeOfContract = this.getFilter(
      initialData.typeOfContract,
      filters.typeOfContract.value,
      FilterTypes.get('type').id,
      FilterTypes.get('type').name
    );
    const system = this.getFilter(
      initialData.system,
      filters.system.value,
      FilterTypes.get('system').id,
      FilterTypes.get('system').name
    );

    // var className = classNames({ Filters: true })

    /*
      return (
         <div className={className}>
            {typeOfContract}
            {system}
            {duration}
            {role}
            {unit}
            <DatesFilter
               onChangeStart={this.onStartDateChange}
               onChangeEnd={this.onEndDateChange} />
         </div>
      );
      */

    return (
      <div>
        <div className="filterRow">{typeOfContract}</div>
        <div className="filterRow">{system}</div>
        <div className="filterRow">{duration}</div>
        <div className="filterRow">{role}</div>
        <div className="filterRow">{unit}</div>
      </div>
    );
  }
}

Filters.propTypes = {
  initialData: PropTypes.object.isRequired,
  filters: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  initialData: state.initialData,
  filters: state.filters
});

const mapDispatchToProps = dispatch => ({
  actions: {
    fetch: bindActionCreators(fetchDataActions, dispatch),
    filter: bindActionCreators(filterChanged, dispatch)
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filters);
