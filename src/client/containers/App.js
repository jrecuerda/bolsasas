import React from 'react';
import { connect } from 'react-redux';
// import classnames from 'classnames';
// import PropTypes from 'prop-types';
// import {Grid, Col} from 'react-bootstrap'

import Filters from './Filters';
import Contracts from './Contracts';

import '../../../public/stylesheets/app.css';

// import Map from '../components/Map'

const App = (initialData, filters) => (
  <div className="App-header container-fluid">
    <div id="header-image">
      <img align="middle" src="logo.jpg" className="App-logo" alt="logo" />
    </div>
    <div id="header-title">Puntuaciones SAS</div>
    <div className="container">
      <div className="row row-eq-height">
        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div id="filters" className="affix">
            <Filters />
          </div>
        </div>
        <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
          <Contracts />
        </div>
      </div>
    </div>
  </div>
);

/*
App.propTypes = {
  initialData: PropTypes.object.isRequired,
  filters: PropTypes.object.isRequired
};
*/

const mapStateToProps = state => ({
  initialData: state.initialData,
  filters: state.filters
});

export default connect(mapStateToProps)(App);
