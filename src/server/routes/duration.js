const express = require('express');

const router = express.Router();

const Duration = require('../models/duration');

router.get('/', (req, res) => {
  Duration.findAll({ raw: true }).then((durations) => {
    res.json(durations);
  });
});

module.exports = router;
