const express = require('express');
const path = require('path');

const app = express();

const roles = require('./roles');
const units = require('./units');
const typeOfPersonal = require('./typeofpersonal');
const system = require('./system');
const duration = require('./duration');
const contracts = require('./contracts');

const router = express.Router();

router.use('/roles', roles);
router.use('/units', units);
router.use('/typeofpersonal', typeOfPersonal);
router.use('/system', system);
router.use('/duration', duration);

router.use('/contracts', contracts);

app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../../..public/index.html'));
});

module.exports = router;
