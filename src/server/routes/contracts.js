const express = require('express');

const router = express.Router();

const Contracts = require('../models/contract');
const Unit = require('../models/unit');
const Role = require('../models/role');
const System = require('../models/system');
const TypeOfPersonal = require('../models/typeofpersonal');
const Duration = require('../models/duration');

const QueryParser = class QueryParser {
  constructor(query) {
    this.query = query;
  }

  static tokenize(value) {
    return value.split(';');
  }

  static addFilterDateLow(query, value) {
    if (typeof value !== 'undefined') {
      query.push({ date: { $gte: value } });
    }
  }

  static addFilterDateUp(query, value) {
    if (typeof value !== 'undefined') {
      query.push({ date: { $lte: value } });
    }
  }

  static getArrayFilter(name, value) {
    return { [name]: { $in: QueryParser.tokenize(value) } };
  }

  static addFilter(query, name, value) {
    if (typeof value !== 'undefined') {
      const filter = QueryParser.getArrayFilter(name, value);
      query.push(filter);
    }
  }

  parse() {
    const ands = [];
    QueryParser.addFilter(ands, 'system_id', this.query.system);
    QueryParser.addFilter(ands, 'type_of_personal_id', this.query.type);
    QueryParser.addFilter(ands, 'duration_id', this.query.duration);
    QueryParser.addFilter(ands, 'role_id', this.query.role);
    QueryParser.addFilter(ands, 'unit_id', this.query.unit);
    QueryParser.addFilterDateLow(ands, this.query.startDate);
    QueryParser.addFilterDateUp(ands, this.query.endDate);
    return { $and: ands };
  }
};

router.get('/', (req, res) => {
  const parser = new QueryParser(req.query);
  Contracts.findAll({
    raw: true,
    limit: 1000,
    include: [
      { model: Unit, as: 'unit', attributes: ['name', 'id', 'lat', 'lon'] },
      { model: Duration, as: 'duration', attributes: ['name', 'id'] },
      { model: TypeOfPersonal, as: 'type_of_personal', attributes: ['name', 'id'] },
      { model: System, as: 'system', attributes: ['name', 'id'] },
      { model: Role, as: 'role', attributes: ['name', 'id'] }
    ],
    attributes: [
      'unit.name',
      'unit.id',
      'unit.lat',
      'unit.lon',
      'role.name',
      'role.id',
      'type_of_personal.name',
      'type_of_personal.id',
      'system.name',
      'system.id',
      'duration.name',
      'duration.id',
      'mark',
      'date'
    ],
    order: [['date', 'DESC']],
    where: parser.parse()
  }).then((contracts) => {
    res.json(contracts);
  });
});

module.exports = router;
