const express = require('express');

const router = express.Router();

const TypeOfPersonal = require('../models/typeofpersonal');

router.get('/', (req, res) => {
  TypeOfPersonal.findAll({ raw: true }).then((types) => {
    res.json(types);
  });
});

module.exports = router;
