const express = require('express');

const router = express.Router();

const Unit = require('../models/unit');

router.get('/', (req, res) => {
  Unit.findAll({ raw: true }).then((units) => {
    res.json(units);
  });
});

module.exports = router;
