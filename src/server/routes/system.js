const express = require('express');

const router = express.Router();

const System = require('../models/system');

router.get('/', (req, res) => {
  System.findAll({ raw: true }).then((system) => {
    res.json(system);
  });
});

module.exports = router;
