const express = require('express');

const router = express.Router();

const Role = require('../models/role');

router.get('/', (req, res) => {
  Role.findAll({ raw: true }).then((roles) => {
    res.json(roles);
  });
});

module.exports = router;
