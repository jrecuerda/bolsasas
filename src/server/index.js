const express = require('express');
const path = require('path');

const index = require('./routes/index');

const app = express();

app.use(express.static('public'));
app.use('/', index);

app.use((req, res, next) => {
  console.error('Request Type/URL:', req.method, ':', req.originalUrl);
  next();
});

// catch 404 and forward to error handler
app.use((req, res) => {
  res.status = 404;
  res.sendFile(path.join(__dirname, '../../public/notfound.html'));
});

app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));
