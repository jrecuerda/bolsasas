const Sequelize = require('sequelize');
const path = require('path');
const fs = require('fs');

const configFilePath = path.join(__dirname, '..', '..', 'privateConfig.json');
// const configFilePath = path.join(__dirname, '..', '..', 'config.json');
const config = JSON.parse(fs.readFileSync(configFilePath, 'utf8'));

const database = config.DBNAME;
const username = config.DBUSER;
const password = config.DBPASSWORD;
const dbhost = config.DBHOST;

const sequelize = new Sequelize(database, username, password, {
  logging: true,
  host: dbhost,
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

sequelize.options.define.timestamps = false;

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });

module.exports = sequelize;
