const Sequelize = require('sequelize');
const sequelize = require('../connector');
const Unit = require('./unit');
const Role = require('./role');
const System = require('./system');
const TypeOfPersonal = require('./typeofpersonal');
const Duration = require('./duration');

const Contract = sequelize.define(
  'contracts',
  {
    type_of_personal_id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    unit_id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    role_id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    duration_id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    system_id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    mark: {
      type: Sequelize.DECIMAL(10, 5),
      primaryKey: true
    },
    date: {
      type: Sequelize.DATE,
      primaryKey: true
    },
    last_check: {
      type: Sequelize.DATE
    },
    num_checks: {
      type: Sequelize.INTEGER
    }
  },
  {
    freezeTableName: true
  }
);

Contract.belongsTo(Unit, { foreignKey: 'unit_id', targetKey: 'id' });
Contract.belongsTo(Role, { foreignKey: 'role_id', targetKey: 'id' });
Contract.belongsTo(TypeOfPersonal, { foreignKey: 'type_of_personal_id', targetKey: 'id' });
Contract.belongsTo(System, { foreignKey: 'system_id', targetKey: 'id' });
Contract.belongsTo(Duration, { foreignKey: 'duration_id', targetKey: 'id' });

module.exports = Contract;
