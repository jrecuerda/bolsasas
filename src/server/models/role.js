const Sequelize = require('sequelize');
const sequelize = require('../connector');

const Role = sequelize.define(
  'role',
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    name: {
      type: Sequelize.TEXT
    }
  },
  {
    freezeTableName: true
  }
);

module.exports = Role;
