const Sequelize = require('sequelize');
const sequelize = require('../connector');

const Duration = sequelize.define(
  'duration',
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    name: {
      type: Sequelize.TEXT
    }
  },
  {
    freezeTableName: true
  }
);

module.exports = Duration;
