const Sequelize = require('sequelize');
const sequelize = require('../connector');

const TypeOfPersonal = sequelize.define(
  'type_of_personal',
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    name: {
      type: Sequelize.TEXT
    }
  },
  {
    freezeTableName: true
  }
);

module.exports = TypeOfPersonal;
