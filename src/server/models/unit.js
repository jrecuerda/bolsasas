const Sequelize = require('sequelize');
const sequelize = require('../connector');

const Unit = sequelize.define(
  'unit',
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    name: {
      type: Sequelize.TEXT
    },
    lat: {
      type: Sequelize.DECIMAL(10, 10)
    },
    lon: {
      type: Sequelize.DECIMAL(10, 10)
    }
  },
  {
    freezeTableName: true
  }
);

module.exports = Unit;
